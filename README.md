## Installation Instructions
1. [ ] Create a copy of env.dist and rename it to .env.
- [ ] Update the .env file to fit server settings.
3. Update DATABASE_URL  value on the .env file to fit server settings
	The following will be needed:
		1. database server version
		2. database
		3. database user with access to database to be used
		4. user's password
		5. IP address/name where the database resides in
		6. This will be the format for DABASE_URL value:
		7.     DATABASE_URL=mysql://user_name:user_pswd@ip_address/db_name?serverVersion=server_version
	[Guide for mysql database servers](#For mysql)
4. Run "composer install" on the command line.
5. If a problem arises such as:
	-Root composer.json requires php (lower version) but your php version (newer version) does not satisfy that requirement.
	You may perform the following:
	1. Update composer.json file to require the current version of php that is running on your machine and run composer install again.

6. Follow the installation instructions
7. Use the DoctrineMigrationBundle using:
8.     php bin/console make:migration
9. If migrations folder is not in the root and is in src or any other directory or has a different name, update the /config/pckages/doctrine_migrations.yaml file:
10. You should see a success message that the migration php file was created. If you open this file, it contains teh SQL needed to update your database! To run that SQL, execute your migrations:
11.     php bin/console doctrine:migrations:migrate
12. You may also run the following instead of having to go through steps 8-11:
13.     bin/console doctrine:schema:update --force
14.  Create a new super admin user with the following command
15.     php bin/console user:create --superadmin <username> <password>

### For mysql
1. Log in as root user by using:
	1.     mysql -u root -p
	2. enter your password
	3. Run The following to create a database
	4.     CREATE DATABASE <database_name>;
	5. To double check if the database has been created you may run:
		1.     SHOW DATABASES;
	6. Create new user
	7.     CREATE USER 'sammy'@'localhost' IDENTIFIED BY 'password';
	8. Grant newly created user with privileges to the the database created
	9.     GRANT ALL PRIVILEGES ON <database_name>.* TO 'sammy'@'localhost' WITH GRANT OPTION;
	10. To check for database server version:
	11.     mysql -V
