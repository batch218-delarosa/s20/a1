// console.log("Testiing");


let providedNumber = Number(prompt("Provide a number greater than 50: "));


if (!providedNumber) {
	console.warn("Please enter a number!");
	throw "Please enter a number!";
}

console.log("The number you provided is " + providedNumber + ".");


for (let i = providedNumber; i >= 0; i--) {
	if (i <= 50) {
		console.log("The current value is at 50. Terminating the loop.")
		break;
	} else if (i % 10 == 0) {
		console.log(`The number is divisible by 10. Skipping the number.`)

	} else if (i % 5 == 0) {
		console.log(i);
	}
}


let string = "supercalifragilisticexpialidocious";

let consonants = "";

for (i = 0; i < string.length; i++) {
	if (
		string[i].toLowerCase() == "a" ||
		string[i].toLowerCase() == "e" ||
		string[i].toLowerCase() == "i" ||
		string[i].toLowerCase() == "o" ||
		string[i].toLowerCase() == "u"
		) {

	} else {
		consonants += string[i];
	}
}

console.log(string);
console.log(consonants);